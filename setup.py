from setuptools import setup, find_packages
import ku

with open("README.md", "r") as f:
    long_description = f.read()
    f.close()

setup(
    name = "ku-proxy",
    version = ku.version,
    author = "seeklay",
    author_email='rudeboy@seeklay.icu',
    url = "https://gitlab.com/seeklay/ku",
    download_url = "https://gitlab.com/seeklay/ku",
    description = "ku is fast, async, modern, little tcp man-in-the-middle proxy library, written in pure Python 3",
    long_description_content_type = "text/markdown",
    long_description = long_description,
    classifiers = [
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: Implementation :: CPython",
        "Programming Language :: Python :: Implementation :: PyPy",
        "Operating System :: OS Independent",
    ],
    platforms = 'OS Independent',
    license = "MIT",
    packages = find_packages(),
    entry_points = {
        'console_scripts': ['kun=ku.kun:entry_point'],
    }
)
