# speed test results
**all tests were run on the same machine, obviously innit?**

## results table
proxy name|Direct|ku|toxic proxy|go tcp proxy
-|-|-|-|-
RPS|747|607|294|425|

## candidates
- ku-proxy v0.1.1 https://gitlab.com/seeklay/ku
- Toxic proxy (asyncio) https://github.com/msoedov/toxic_proxy
- go tcp proxy (golang) https://github.com/jpillora/go-tcp-proxy

## go tcp proxy
this proxy doesn't have a disable logging flag, and printing to the console probably affects its performance in tests, but the result is good anyway

## toxic proxy script
```
import asyncio
from toxic_proxy import toxic_proxy

proxy = toxic_proxy(destination=('lc', 8080), port=80)


loop = asyncio.get_event_loop()
server = loop.run_until_complete(proxy)
print('Serving on {}'.format(server.sockets[0].getsockname()))
try:
    loop.run_forever()
except KeyboardInterrupt:
    pass
# Close the server
server.close()
loop.run_until_complete(server.wait_closed())
loop.close()
```
## testing target
```bash
php -S lc:8080
```

## testing tool
```bash
ab.exe -n 10000 -c 50 http://lc:8080/
```
 ```
 This is ApacheBench, Version 2.3 <$Revision: 1903618 $>
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
Licensed to The Apache Software Foundation, http://www.apache.org/
*lc:8080 = php -S lc:8080*
```

### direct connection
```
Server Software:
Server Hostname:        lc
Server Port:            8080

Document Path:          /
Document Length:        533 bytes

Concurrency Level:      50
Time taken for tests:   13.378 seconds
Complete requests:      10000
Failed requests:        0
Non-2xx responses:      10000
Total transferred:      6910000 bytes
HTML transferred:       5330000 bytes
Requests per second:    747.48 [#/sec] (mean)
Time per request:       66.891 [ms] (mean)
Time per request:       1.338 [ms] (mean, across all concurrent requests)
Transfer rate:          504.40 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0    0   2.2      0      47
Processing:    16   66  13.7     63     109
Waiting:       16   65  13.6     63     109
Total:         16   66  13.7     63     125

Percentage of the requests served within a certain time (ms)
  50%     63
  66%     63
  75%     74
  80%     78
  90%     94
  95%     94
  98%     94
  99%     98
 100%    125 (longest request)
```


### speedtest.py (ku)
```
Server Software:
Server Hostname:        lc
Server Port:            80

Document Path:          /
Document Length:        533 bytes

Concurrency Level:      50
Time taken for tests:   16.468 seconds
Complete requests:      10000
Failed requests:        0
Non-2xx responses:      10000
Total transferred:      6860000 bytes
HTML transferred:       5330000 bytes
Requests per second:    607.23 [#/sec] (mean)
Time per request:       82.342 [ms] (mean)
Time per request:       1.647 [ms] (mean, across all concurrent requests)
Transfer rate:          406.79 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0    0   2.5      0      16
Processing:    31   81   7.4     78     114
Waiting:        0   80   7.0     78     114
Total:         31   82   7.5     78     114

Percentage of the requests served within a certain time (ms)
  50%     78
  66%     78
  75%     85
  80%     94
  90%     94
  95%     94
  98%     94
  99%     95
 100%    114 (longest request)
```

### toxic proxy
```
Server Software:
Server Hostname:        lc
Server Port:            80

Document Path:          /
Document Length:        533 bytes

Concurrency Level:      50
Time taken for tests:   33.997 seconds
Complete requests:      10000
Failed requests:        0
Non-2xx responses:      10000
Total transferred:      6860000 bytes
HTML transferred:       5330000 bytes
Requests per second:    294.14 [#/sec] (mean)
Time per request:       169.987 [ms] (mean)
Time per request:       3.400 [ms] (mean, across all concurrent requests)
Transfer rate:          197.05 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0    0   2.1      0      16
Processing:    47  169  11.6    172     219
Waiting:       31  148  11.3    141     203
Total:         47  169  11.6    172     219

Percentage of the requests served within a certain time (ms)
  50%    172
  66%    172
  75%    172
  80%    172
  90%    188
  95%    188
  98%    203
  99%    203
 100%    219 (longest request)
```

### go tcp proxy
```
Server Software:
Server Hostname:        lc
Server Port:            80

Document Path:          /
Document Length:        533 bytes

Concurrency Level:      50
Time taken for tests:   23.480 seconds
Complete requests:      10000
Failed requests:        0
Non-2xx responses:      10000
Total transferred:      6860000 bytes
HTML transferred:       5330000 bytes
Requests per second:    425.89 [#/sec] (mean)
Time per request:       117.401 [ms] (mean)
Time per request:       2.348 [ms] (mean, across all concurrent requests)
Transfer rate:          285.31 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0    1   0.4      1       3
Processing:     6  116  30.1    116     343
Waiting:        3   85  34.8     79     244
Total:          6  117  30.1    116     343

Percentage of the requests served within a certain time (ms)
  50%    116
  66%    121
  75%    126
  80%    129
  90%    151
  95%    168
  98%    193
  99%    213
 100%    343 (longest request)
```