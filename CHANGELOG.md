# v0.1.0-RELEASE (Jan 31 2023)
* Initial release
* Fixed funny error: shared file descriptors and sessions lists between separated proxy instances

# v0.1.1 (Feb 1 2023)
* add resources release in `shutdown()`
* add tests

# v0.1.2
* add args for sessions
* fix port as str in host addr tuple
* add broken session error

# v0.2.0 GLOBAL REWRTIE (Jul 2 2023)
* add states to tcpsession
* add SO_CONDITIONAL_ACCEPT (windows only)
* new, clean, scallable, architecture
* new cli proxy utility (beta(alpha)) - kun
* add async client accept (!!!)
* unchanged basics: tcp, mitm, tcpsession, ood
* add multiaddr listening
* add daemon=True to poll thread, if you forget to shutdown proxy, you still can exit program

# v0.2.1 (Jul 8 2023)
* hide `logger` and `state`

# v0.2.2-pre1 (Jul 13 2023)
* add multi-threaded parallelism support
* add another screen for kun in transit mode (performance) use arrows on keyboard to move between screens
* add --nt to kun to set number of polling threads to create (default 1)