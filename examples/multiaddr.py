from ku import ku, util, tcpsession
from time import sleep
import logging
import re

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
stdout_handler = logging.StreamHandler()
stdout_handler.setLevel(logging.DEBUG)
stdout_handler.setFormatter(util.CustomFormatter('[%(asctime)s] [%(levelname)s] %(name)s:  %(message)s'))
logger.addHandler(stdout_handler)

# enabling low level tcp debug logging
logging.getLogger("ku.devel").setLevel(logging.DEBUG)

listen = [
    "[::1]", 80,
    "localhost", 80 # domain names only ipv4! but you can resolve it into addresess by yourself
]

class conn(tcpsession):
  
    def serverbound(self, data):        
        data = re.sub("Host: (.*)\r\n", 'Host: api.ipify.org\r\n', data.decode())
        return data.encode()

proxy = ku(listen, ("api.ipify.org", 80), conn)
print(f"Listening: {[s.getsockname() for s in proxy.sockets]}")
#proxy = ku(("[::]", 80), ("[::1]", 80)) ipv6 ready! just enclose ipv6 addr in [brackets]
# now the proxy is already running

while 7:
    try:
        sleep(0.07)
    except KeyboardInterrupt:
        proxy.shutdown() #proxy creates a thread to async poll for socket events
        break            #we need to call shutdown() to break the thread loop