from ku import ku, tcpsession
from time import sleep

proxy = ku(("localhost", 80, "[::1]", 80), ("g.co", 80))

while 7:
    try:
        sleep(0.07)
    except KeyboardInterrupt:
        proxy.shutdown() #proxy creates a thread to async poll for socket events
        break            #we need to call shutdown() to break the thread loop