from ku import ku, tcpsession, Pass, Reject
from time import sleep

class conn(tcpsession):

    def __init__(self, client, server, proxy):
        self.client = client
        self.server = server
        self.proxy = proxy
        self.id = id(self)        
        print(F"#{self.id} new conn {client.getpeername()}->{client.getsockname()}::{server.getsockname()}->{server.getpeername()}")

    def clientbound(self, data):        
        print(F"#{self.id} server->client  {len(data)}")
        print(data)
        self.terminate()
        # drop connection, when server want to respond
    
    def serverbound(self, data):        
        print(F"#{self.id} client->server  {len(data)}")
        print(data)
    
    def connection_made(self):
        print(F"#{self.id} connection_made")
    
    def connection_lost(self, side, err):
        side = 'client' if side is self.client else 'server' if side is not None else 'proxy'
        print(F"#{self.id} connection_lost by {side} due to {err}")

print("Starting...")
proxy = ku(("localhost", 80), ("api.ipify.org", 80), conn)
print("Started")

while 1:
    try:
        sleep(0.07)
    except KeyboardInterrupt:
        print("Shutting down...")
        proxy.shutdown()
        print("Exiting...")
        break