from ku import ku
import requests

def test_multiinstance():
    remote = "http://ip-api.com/line/"
    direct = requests.get(remote, timeout=5).content
    proxies = [ku(("localhost", i), ("ip-api.com", 80)) for i in range(80, 130, 10)]

    def get(p):
        print(F'Sending to {p}')
        r = requests.get(F"http://localhost:{p}/line", headers={'host': 'ip-api.com'}, timeout=5).content
        print(F'{p} done')
        return r

    try:
        for i in range(80, 130, 10):
            assert get(i) == direct

    finally:
        # shutdown
        for p in proxies:
            p.shutdown()

def test_chain():
    remote = "http://ip-api.com/line/"
    direct = requests.get(remote, timeout=5).content
    proxies = []
    proxies.append(ku(("localhost", 100), ("localhost", 110)))
    proxies.append(ku(("localhost", 110), ("localhost", 120)))
    proxies.append(ku(("localhost", 120), ("ip-api.com", 80)))

    def get(p):
        print(F'Sending to {p}')
        r = requests.get(F"http://localhost:{p}/line", headers={'host': 'ip-api.com'}, timeout=5).content
        print(F'{p} done')
        return r

    try:
        for i in range(100, 121, 10):
            assert get(i) == direct

    finally:
        # shutdown
        for p in proxies:
            p.shutdown()    