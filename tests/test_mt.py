import requests
from ku import ku, tcpsession

def test_mt():

    rhost, rport = "api.ipify.org", 80
    headers = {'host': rhost}
    print("Sending direct request...")
    direct_request_content = requests.get(f"http://{rhost}", headers=headers).content
    print("Direct response: ", direct_request_content)
    a = {}

    class test_conn(tcpsession):

        def __init__(self, client, server, proxy, a):
            self.client = client
            self.server = server
            self.proxy = proxy
            self.id = id(self)  
            self.closed = False
            self.a = a
            self.a["closed"] = False
            self.a["all_from_server"] = b""

        def clientbound(self, data):
            self.a["all_from_server"] += data
    
        def connection_lost(self, side, err):
            self.a["closed"] = True
    
    print("Startng proxy...")
    proxy = ku(("localhost", 8080), (rhost, rport), test_conn, (a,), parallelism=4)

    print("Sending request via proxy...")
    proxied_request_content = requests.get(f"http://localhost:8080/", headers=headers).content
    print("Proxied response: ", proxied_request_content)

    while not a["closed"]:
        pass

    proxy.shutdown()
    catched_content = a["all_from_server"].split(b"\r\n\r\n")[1]
    print("Catched on proxy content: ", catched_content)

    assert direct_request_content == proxied_request_content == catched_content    