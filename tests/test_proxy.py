from ku import ku, tcpsession, Pass, Reject
import requests

def test_proxy():

    rhost, rport = "api.ipify.org", 80
    headers = {'host': rhost}
    print("Sending direct request...")
    direct_request_content = requests.get(f"http://{rhost}", headers=headers).content
    print("Direct response: ", direct_request_content)
    a = {}

    class test_conn(tcpsession):

        def __init__(self, client, server, proxy, a):
            self.client = client
            self.server = server
            self.proxy = proxy
            self.id = id(self)  
            self.closed = False
            self.a = a
            self.a["closed"] = False
            self.a["all_from_server"] = b""

        def clientbound(self, data):
            self.a["all_from_server"] += data
    
        def connection_lost(self, side, err):
            self.a["closed"] = True
    
    print("Startng proxy...")
    proxy = ku(("localhost", 8080), (rhost, rport), test_conn, (a,))

    print("Sending request via proxy...")
    proxied_request_content = requests.get(f"http://localhost:8080/", headers=headers).content
    print("Proxied response: ", proxied_request_content)

    while not a["closed"]:
        pass

    proxy.shutdown()
    catched_content = a["all_from_server"].split(b"\r\n\r\n")[1]
    print("Catched on proxy content: ", catched_content)

    assert direct_request_content == proxied_request_content == catched_content

def test_spoof():

    rhost, rport = "api.ipify.org", 80
    headers = {'host': rhost}
    print("Sending direct request...")
    direct_request_content = requests.get(f"http://{rhost}").content
    print("Direct response: ", direct_request_content)
    a = {}

    class test_conn(tcpsession):

        def __init__(self, client, server, proxy, a):
            self.client = client
            self.server = server
            self.proxy = proxy
            self.id = id(self)  
            self.a = a
            self.a["closed"] = False
            self.a["all_from_server"] = b""      
            self.a["spoofed_with"] = b""

        def clientbound(self, data):
            self.a["all_from_server"] += data
            cont = "Hello, this is the message from the proxy!"
            self.a["spoofed_with"] = cont.encode()
            fake_response = "HTTP/1.1 200 OK\r\n"
            fake_response += f"Content-length: {len(cont)}\r\n\r\n"
            fake_response += cont

            return fake_response.encode()
    
        def connection_lost(self, side, err):
            self.a["closed"] = True
    
    print("Startng proxy...")
    proxy = ku(("localhost", 8080), (rhost, rport), test_conn, (a,))

    print("Sending request via proxy...")
    proxied_request_content = requests.get(f"http://localhost:8080/",  headers=headers).content
    print("Proxied response: ", proxied_request_content)

    while not a["closed"]:
        pass

    proxy.shutdown()
    catched_content = a["all_from_server"].split(b"\r\n\r\n")[1]
    print("Catched on proxy content: ", catched_content)

    assert direct_request_content == catched_content
    assert proxied_request_content == a["spoofed_with"]

def test_terminate():

    class test_conn(tcpsession):

        def __init__(self, client, server, proxy):
            self.client = client
            self.server = server
            self.proxy = proxy
            self.id = id(self)  

        def serverbound(self, data):
            self.client.send(b"HTTP/1.0 200 OK\r\n\r\nterminate")
            self.proxy.terminate(self)
    
    print("Startng proxy...")
    proxy = ku(("localhost", 8080), ("g.co", 80), test_conn)

    print("Sending request via proxy...")
    proxied_request_content = requests.get(f"http://localhost:8080/").content
    print("Proxied response: ", proxied_request_content)

    proxy.shutdown()

    assert proxied_request_content == b"terminate"

def test_reject():

    class test_conn(tcpsession):

        def __init__(self, client, server, proxy):
            self.client = client
            self.server = server
            self.proxy = proxy
            self.id = id(self)  

        def serverbound(self, data):
            return Reject

    print("Startng proxy...")
    proxy = ku(("localhost", 8090), ("g.co", 80), test_conn)

    print("Sending request via proxy...")
    try:
        proxied_request_content = requests.get(f"http://localhost:8090/", timeout=5).content
    except requests.exceptions.ReadTimeout as e:
        print("Hahaha, Timeout?", e)
        proxy.shutdown()
        return
    raise RuntimeError("No timeout!?")